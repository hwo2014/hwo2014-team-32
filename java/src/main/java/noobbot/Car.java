package noobbot;

import java.util.ArrayList;
import java.util.List;

/** car, identification and position classes */

public class Car {
	// json model attributes
	public CarId id;
	public CarDimensions dimensions;
	public CarPosition position;

	// other attributes
	/** car speed, in length units per game tick */
	public double speed;

	/** car acceleration, in length units per game tick ^ 2 */
	public double acceleration;

	/** car slip angular speed, in degrees per game tick */
	public double slipSpeed;

	/** car slip angular acceleration, in degrees per game tick ^ 2 */
	public double slipAcceleration;

	/** car throttle state */
	public double throttle;

	/** car recommended max speed for the latest position */
	public double recommendedSpeed;

	/** crashed */
	public boolean crashed;
	
	/** turbo list */
	public List<Turbo> turboList = new ArrayList<Turbo>();

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof Car))
			return false;
		Car otherCar = (Car) other;

		if (this.id != null && otherCar.id != null) {
			return this.id.equals(otherCar.id);
		}
		return false;
	}

	/** get available turbo or null */
	public Turbo getAvailableTurbo() {
		for (Turbo t : turboList) {
			if (t.activationTick <= 0) {
				return t;
			}
		}

		return null;
	}

	/** activate turbo */
	public void activateTurbo(Turbo turbo, long gameTick) {
		for (Turbo t : turboList) {
			// set activatedTick to gameTick to signal turbo activation
			if (t.availableTick == turbo.availableTick) {
				t.activationTick = gameTick;
				Util.log("TURBO!");
			}
		}
	}

	/** check if turbo is active */
	public boolean hasActiveTurbo(long gameTick) {
		for (Turbo t : turboList) {
			if (t.activationTick > 0 && gameTick > t.activationTick && gameTick <= (t.durationTicks + t.activationTick)) {
				return true;
			}
		}
		return false;
	}
}

class CarId {
	// json model attributes
	private String name;
	private String color;

	@Override
	public boolean equals(Object other) {
		if (other == null)
			return false;
		if (other == this)
			return true;
		if (!(other instanceof CarId))
			return false;
		CarId otherId = (CarId) other;

		// return true if name and color match
		if (this.name != null && otherId.name != null && this.name.equals(otherId.name) && this.color != null && otherId.color != null && this.color.equals(otherId.color)) {
			return true;
		}

		return false;
	}

	@Override
	public String toString() {
		return this.name + " (" + this.color + ")";
	}

}

class CarDimensions {
	// json model attributes
	public double length;
	public double width;
	public double guidePosition;
}

class CarPosition {
	// json model attributes
	public CarId id;
	public double angle;
	public PiecePosition piecePosition;
}

class PiecePosition {
	// json model attributes
	public int pieceIndex;
	public double inPieceDistance;
	public PiecePositionLane lane;
	public int lap;
}

class PiecePositionLane {
	// json model attributes
	public int startLaneIndex;
	public int endLaneIndex;
}

class Turbo {
	// json model attributes
	public long durationTicks;
	public double turboFactor;

	// other attributes
	/** tick when turbo became available */
	public long availableTick;
	/** tick when turbo became active */
	public long activationTick = -1L;

	Turbo(long availableTick, long durationTicks, double turboFactor) {
		this.durationTicks = durationTicks;
		this.availableTick = availableTick;
		this.turboFactor = turboFactor;
	}
}