package noobbot;

import java.text.SimpleDateFormat;
import java.util.Date;

/** class with auxiliary methods for logging, string manipulation, etc. */
public class Util {

	/** time format */
	public static final String TIME_FORMAT = "mm:ss.SSS";

	/** formats time to string */
	public static String formatTime(Date date) {
		if (date != null) {
			return new SimpleDateFormat(TIME_FORMAT).format(date);
		} else {
			return "";
		}
	}

	/** simple log */
	public static void log(String str) {
		log(null, null, str);
	}

	/** log with time stamp prefix : uses current time since startDate */
	public static void log(Date startDate, String str) {
		log(startDate, new Date(), str);
	}

	/** log with time stamp prefix */
	public static void log(Date startDate, Date now, String str) {
		if (startDate != null && now != null) {
			System.out.println(formatTime(new Date(now.getTime() - startDate.getTime())) + " : " + str);
		} else {
			System.out.println(str);
		}
	}
}
