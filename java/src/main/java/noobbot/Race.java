package noobbot;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** race and race session classes */
public class Race {
	// json model attributes
	public Track track;
	public List<Car> cars;
	public RaceSession raceSession;

	// other attributes
	public Map<Integer, Double> speedBoostsByTrackPiece = new HashMap<Integer, Double>();
	public Map<Integer, Set<Integer>> speedBoostSetsByLap = new HashMap<Integer, Set<Integer>>();
	public Car myCar;

	// constants
	public static final double CRASH_ANGLE = 60d;
	public static final double MAX_ANGLE = 55d;
	public static final double SPEED_BOOST_FACTOR = 0.15d;
	public static final double CHECK_LENGTH = 400d;
	public static final double MAX_SPEED = 99999.0d; // max speed assumed to be infinite on straight pieces
	public static final double CURVE_COUNTER_SLIP_SPEED_MOD = 1.2d;
	public static final double CURVE_COUNTER_SLIP_ANGLE = 15d;
	public static final double CRASH_SLIP_FRICTION_MOD = 0.90d;
	public static final double OBSTACLE_SPEED_THRESHOLD = 0.90d;

	// parameters that may change for each track and car
	double kSlipFriction = 0.40d; // higher allows faster speed during curves
	double kFrontDrag = 0.020d; // higher allows faster speed before curves
	double kPowerMod = 1;

	// double kSlipFriction = 0.42d; // higher allows faster speed during curves
	// double kFrontDrag = 0.022d; // higher allows faster speed

	/** get car by id */
	public Car getCar(CarId id) {
		for (Car c : this.cars) {
			if (c.id.equals(id)) {
				return c;
			}
		}

		return null;
	}

	/** returns true if there is a slower car on the same lane in the pieces in front of my car */
	public boolean myCarLaneHasObstacle(int myLaneIdx) {
		if (this.track == null || this.myCar == null || this.myCar.position == null || this.myCar.position.piecePosition == null) {
			return false;
		}

		// check if each other car is in front of you, in the same lane, moving slower than he should, or crashed
		PiecePosition myPiecePosition = this.myCar.position.piecePosition;
		int myPieceIdx = myPiecePosition.pieceIndex;

		int laneIdx = 0;
		int pieceIdx = 0;
		int checkPieceIdx = 0;
		for (Car c : this.cars) {
			if (!c.equals(this.myCar) && c.position != null && c.position.piecePosition != null) {
				laneIdx = c.position.piecePosition.lane.endLaneIndex;
				pieceIdx = c.position.piecePosition.pieceIndex;

				checkPieceIdx = myPieceIdx;
				if (laneIdx == myLaneIdx && (c.speed < OBSTACLE_SPEED_THRESHOLD * c.recommendedSpeed || c.crashed)) {
					for (int i = 0; i < 4; i++) {

						if (checkPieceIdx == pieceIdx) {
							return true;
						}
						checkPieceIdx = (checkPieceIdx + 1) % this.track.pieces.size();
					}
				}
			}
		}

		return false;
	}

	/** returns true if my car can switch to a lane */
	public boolean canMyCarSwitchToLane(int targetLaneIdx) {
		if (this.track == null || this.myCar == null || this.myCar.position == null || this.myCar.position.piecePosition == null) {
			return false;
		}

		// check if target lane is invalid
		if (targetLaneIdx < 0 || targetLaneIdx >= this.track.lanes.size()) {
			return false;
		}

		// check if target lane has obstacle
		if (myCarLaneHasObstacle(targetLaneIdx)) {
			return false;
		}

		PiecePosition myPiecePosition = this.myCar.position.piecePosition;
		int myPieceIdx = myPiecePosition.pieceIndex;
		int myLaneIdx = myPiecePosition.lane.endLaneIndex;

		int previousPieceIdx = myPiecePosition.pieceIndex - 1;
		if (previousPieceIdx < 0) {
			previousPieceIdx = track.pieces.size() - 1;
		}

		int laneIdx = 0;
		int pieceIdx = 0;
		for (Car c : this.cars) {
			if (!c.equals(this.myCar) && c.position != null && c.position.piecePosition != null) {
				laneIdx = c.position.piecePosition.lane.endLaneIndex;
				pieceIdx = c.position.piecePosition.pieceIndex;

				// if there is a car between, return false
				if ((pieceIdx == myPieceIdx) || (pieceIdx == previousPieceIdx)) {

					if (myLaneIdx < targetLaneIdx && myLaneIdx < laneIdx) {
						return false;
					} else if (myLaneIdx > targetLaneIdx && myLaneIdx > laneIdx) {
						return false;
					}
				}
			}
		}

		return true;
	}

	/** update car movement data : position, speed, acceleration */
	public void updateCarMovementData(List<CarPosition> newCarPositions) {

		for (CarPosition newCarPosition : newCarPositions) {
			Car c = getCar(newCarPosition.id);

			if (c != null) {
				// calculate speed and acceleration, assume time difference is 1 tick
				double newSpeed = 0d;
				double newAcceleration = 0d;
				double newSlipSpeed = 0d;
				// double newSlipAcceleration = 0d;
				if (c.position != null) {
					int oldPieceIndex = c.position.piecePosition.pieceIndex;
					int newPieceIndex = newCarPosition.piecePosition.pieceIndex;
					int oldLaneIndex = c.position.piecePosition.lane.endLaneIndex;

					if (oldPieceIndex == newPieceIndex) {
						newSpeed = newCarPosition.piecePosition.inPieceDistance - c.position.piecePosition.inPieceDistance;
					} else {
						double oldPosition = c.position.piecePosition.inPieceDistance;
						double oldPieceLength = track.pieces.get(c.position.piecePosition.pieceIndex).getLengthForLane(oldLaneIndex);

						if (oldPosition > oldPieceLength) {
							Util.log("WARNING : old car position longer than piece length : oldPosition=" + oldPosition + " oldPieceLength=" + oldPieceLength);
							newSpeed = newCarPosition.piecePosition.inPieceDistance - 0;
						} else {
							newSpeed = newCarPosition.piecePosition.inPieceDistance + (oldPieceLength - oldPosition);
						}

						if (newSpeed < 0) {
							Util.log("WARNING : speed calc fail : OLD length=" + oldPieceLength + " inPieceDst=" + oldPosition + " NEW inPieceDst=" + newCarPosition.piecePosition.inPieceDistance);
						}
					}
					newAcceleration = newSpeed - c.speed;
					newSlipSpeed = newCarPosition.angle - c.position.angle;
					// newSlipAcceleration = newSlipSpeed - c.slipSpeed;

					int lap = newCarPosition.piecePosition.lap;
					Set<Integer> boostedPieceIndexes = speedBoostSetsByLap.get(lap);
					if (boostedPieceIndexes == null) {
						boostedPieceIndexes = new HashSet<Integer>();
						speedBoostSetsByLap.put(lap, boostedPieceIndexes);

					}

					// only consider boost once per lap per piece
					// boosts are calculated taking the players car as reference
					// they only affect speed restriction on curves
					if (c.equals(this.myCar) && !boostedPieceIndexes.contains(newPieceIndex)) {
						// if considerably positive or negative and angular speed crossed zero, a maximum angle has been found
						// only add boost if near recommended speed
						if ((newCarPosition.angle > 5d || newCarPosition.angle < -5d) && (newSpeed >= 0.98 * c.recommendedSpeed)
								&& ((newSlipSpeed <= 0 && c.slipSpeed > 0) || (newSlipSpeed >= 0 && c.slipSpeed < 0))) {

							double oldSpeedBoost = 1d;
							double newSpeedBoost = 1d;
							if (speedBoostsByTrackPiece.containsKey(newPieceIndex)) {
								oldSpeedBoost = speedBoostsByTrackPiece.get(newPieceIndex);
							}

							newSpeedBoost = oldSpeedBoost + (1 - Math.abs(newCarPosition.angle) / MAX_ANGLE) * SPEED_BOOST_FACTOR;
							Util.log("SPEEDBOOST near pieceIdx=" + newPieceIndex + " oldBoost=" + oldSpeedBoost + " newBoost=" + newSpeedBoost + " spd%=" + newSpeed / c.recommendedSpeed
									+ " slipAngle=" + newCarPosition.angle);

							// store speed increment for this piece and nearby pieces
							int idx = newPieceIndex;
							for (int i = 0; i < 3; i++) {
								idx = newPieceIndex - i;
								if (idx < 0) {
									idx = track.pieces.size() - idx;
								}
								speedBoostsByTrackPiece.put(idx, newSpeedBoost);
								boostedPieceIndexes.add(idx);
							}
						}
					}
				}

				// update car values
				c.position = newCarPosition;
				c.speed = newSpeed;
				c.acceleration = newAcceleration;
				c.slipSpeed = newSlipSpeed;
			}
		}
	}

	/** calculate track piece length and radius for each lane for curves */
	public void loadCurvePiecesLength() {
		if (track != null && track.pieces != null) {
			for (TrackPiece tp : track.pieces) {
				if (tp.isCurve()) {
					for (TrackLane tl : track.lanes) {
						tp.calcCurveDataForLane(tl.index, tl.distanceFromCenter);
					}
				}
			}
		}
	}

	/** get recommended max speed for car */
	public double getRecommendedSpeedForCar(Car car) {
		if (car != null && car.position != null && car.position.piecePosition != null) {
			return getRecommendedSpeedForPosition(car.position.piecePosition.pieceIndex, car.position.piecePosition.lane.endLaneIndex, car.position.piecePosition.inPieceDistance, car.position.angle);
		}
		return 0d;
	}

	/** get recommended max speed for position */
	public double getRecommendedSpeedForPosition(int trackPieceIndex, int laneIndex, double inTrackPieceDistance, double angle) {

		// auxiliary equations
		// Vmax = sqrt(r*kSlipFriction)
		// Vcurrent = (Sdist*kFrontDrag +- sqrt ((Sdist*kFrontDrag)^2 - 4*(-Vmax^2-Sdist*kFrontDrag*Vmax))) / 2

		double result = MAX_SPEED;

		// get current track piece type
		TrackPiece currentPiece = this.track.pieces.get(trackPieceIndex);

		// check restriction due to being on a curve, if applicable
		if (currentPiece.isCurve() && inTrackPieceDistance < 0.99 * currentPiece.getLengthForLane(laneIndex)) {
			result = (double) Math.min(result, Math.sqrt(currentPiece.getRadiusForLane(laneIndex) * kSlipFriction));

			// if sliping in the opposite direction, assume speed can be a bit higher
			if ((angle > -CURVE_COUNTER_SLIP_ANGLE && currentPiece.angle < 0) || (angle < CURVE_COUNTER_SLIP_ANGLE && currentPiece.angle > 0)) {
				result = result * CURVE_COUNTER_SLIP_SPEED_MOD;
			}
			// apply speed boost, if any
			if (speedBoostsByTrackPiece.containsKey(trackPieceIndex)) {
				result = result * speedBoostsByTrackPiece.get(trackPieceIndex);
			}
		}

		// check pieces ahead, until check length and at least one curve is found
		double totalLength = currentPiece.getLengthForLane(laneIndex) - inTrackPieceDistance;
		int nextPieceIndex = (trackPieceIndex + 1) % track.pieces.size();
		double length = 0d;
		double distanceToPiece = 0d;
		// double restrictionFactor = 0d;

		while (totalLength < CHECK_LENGTH || result == MAX_SPEED) {
			TrackPiece nextPiece = track.pieces.get(nextPieceIndex);
			length = nextPiece.getLengthForLane(laneIndex);
			if (length < 0d) {
				Util.log("WARNING : invalid length for pieceIdx=" + nextPieceIndex + " and laneIdx=" + laneIndex);
				break;
			}

			// apply curve restriction
			if (nextPiece.isCurve()) {

				// curves that are still far away should be less restrictive
				distanceToPiece = totalLength;

				double vMax = Math.sqrt(nextPiece.getRadiusForLane(laneIndex) * kSlipFriction);
				double b = -distanceToPiece * kFrontDrag;
				double v1 = (-b + Math.sqrt(Math.pow(b, 2) - 4 * (-Math.pow(vMax, 2) + b * vMax))) / 2;
				double v2 = (-b - Math.sqrt(Math.pow(b, 2) - 4 * (-Math.pow(vMax, 2) + b * vMax))) / 2;

				// apply speed boost, if any
				if (speedBoostsByTrackPiece.containsKey(nextPieceIndex)) {
					v1 = v1 * speedBoostsByTrackPiece.get(nextPieceIndex);
					v2 = v2 * speedBoostsByTrackPiece.get(nextPieceIndex);
				}

				// Util.log(" v1=" + v1 + " v2=" + v2);
				result = Math.min(result, Math.max(Math.abs(v1), Math.abs(v2)));
			}

			nextPieceIndex = (nextPieceIndex + 1) % track.pieces.size();
			totalLength += length;
		}

		return result;
	}

	/** get lane that leads to the fastest laps (estimate) */
	public int getPreferredLaneIndex() {
		int result = 0;
		double bestTime = 999999.0d;

		for (TrackLane tl : this.track.lanes) {
			double length = getTotalLengthForLane(tl.index);
			double avgSpeed = getAvgSpeedForLane(tl.index);
			double estTime = length / avgSpeed;

			Util.log("lane " + tl.index + " : len=" + length + " spd=" + avgSpeed + " time=" + estTime);

			if (estTime < bestTime) {
				bestTime = estTime;
				result = tl.index;
			}
		}

		Util.log("best lane index is " + result);
		return result;
	}

	/** get total length for lane */
	public double getTotalLengthForLane(int laneIndex) {
		double result = 0d;
		if (this.track != null) {
			List<TrackPiece> trackPieces = this.track.pieces;
			for (TrackPiece tp : trackPieces) {
				result += tp.getLengthForLane(laneIndex);
			}
		}

		return result;
	}

	/** get average speed for lane (estimate) */
	public double getAvgSpeedForLane(int laneIndex) {
		double result = 0d;
		if (this.track != null) {
			List<TrackPiece> trackPieces = this.track.pieces;
			for (int i = 0; i < trackPieces.size(); i++) {
				result += getAvgSpeedForPieceAndLane(i, laneIndex);
			}

			result = result / trackPieces.size();
		}

		return result;
	}

	/** get average speed for track piece at lane (estimate) */
	public double getAvgSpeedForPieceAndLane(int pieceIndex, int laneIndex) {
		double result = 0d;
		if (this.track != null) {
			TrackPiece tp = this.track.pieces.get(pieceIndex);

			double length = tp.getLengthForLane(laneIndex);
			for (int i = 0; i < 10; i++) {
				result += getRecommendedSpeedForPosition(pieceIndex, laneIndex, length * (1d + i) / 10d, 0d);
			}

			result = result / 10;
		}

		return result;
	}
}

class RaceSession {
	// json model attributes
	public int laps;
	public long maxLapTimeMs;
	public boolean quickRace;

}
