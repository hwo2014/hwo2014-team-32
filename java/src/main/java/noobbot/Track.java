package noobbot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/** track related classes */
public class Track {

	// json model attributes
	/** track id */
	public String id;

	/** track name */
	public String name;

	/** lanes : 1 to N */
	public List<TrackLane> lanes;

	/** track pieces */
	public List<TrackPiece> pieces = null;

	/** starting point */
	public StartingPoint startingPoint = null;
}

class StartingPoint {
	// json model attributes
	public Position position;
	public double angle;
}

class Position {
	// json model attributes
	public double x;
	public double y;
}

class TrackLane {
	// json model attributes
	public int index;
	public double distanceFromCenter;
}

class TrackPiece {
	// json model attributes
	public boolean switchLane;
	public double radius;
	public double angle;
	public double length;

	// other attributes
	public int index;
	private Map<Integer, Double> curveLengthByLaneIndex = new HashMap<Integer, Double>();
	private Map<Integer, Double> radiusByLaneIndex = new HashMap<Integer, Double>();

	public void calcCurveDataForLane(int laneIndex, double distanceFromCenter) {
		if (isCurve()) {
			this.radiusByLaneIndex.put(laneIndex, (this.radius + (this.angle > 0 ? -distanceFromCenter : distanceFromCenter)));
			this.curveLengthByLaneIndex.put(laneIndex, Math.abs((this.angle / 180d) * (double) Math.PI * this.radiusByLaneIndex.get(laneIndex)));
		}
	}

	/** returns length for lane, or -1 if lane is invalid */
	public double getLengthForLane(int laneIndex) {
		if (isCurve()) {
			if (this.curveLengthByLaneIndex.containsKey(laneIndex)) {
				return this.curveLengthByLaneIndex.get(laneIndex);
			} else {
				Util.log("WARNING : tried to get curve length for invalid lane : " + laneIndex);
				return -1d;
			}
		}
		return this.length;
	}

	/** returns radius for lane, or -1 if lane is invalid */
	public double getRadiusForLane(int laneIndex) {
		if (isCurve() && this.radiusByLaneIndex.containsKey(laneIndex)) {
			return this.radiusByLaneIndex.get(laneIndex);
		} else {
			Util.log("WARNING : tried to get curve radius for invalid lane : " + laneIndex);
			return -1d;
		}
	}

	public boolean isCurve() {
		return this.radius > 0;
	}

}