package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
//import java.lang.reflect.Type;
import java.net.Socket;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;

//import com.google.gson.reflect.TypeToken;

/** main and client<->server message classes */
public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		Util.log("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

		new Main(reader, writer, new Join(botName, botKey));

		socket.close();
	}

	final Gson gson = new Gson();

	private PrintWriter writer;

	public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
		this.writer = writer;
		String line = null;
		boolean logMovementData = false;

		// race related variables
		Race race = null;
		CarId myCarId = null;
		Car myCar = new Car();
		Date startTime = null;
		double maxSlipAngle = 0d;
		boolean checkedFrontDrag = false;
		boolean checkedEfPower = false;

		// connect to server
		send(join);

		// BotId botId = new BotId(join.name, join.key);
		// JoinRace jr = new JoinRace(botId, "keimola", "xpto", 1);
		// JoinRace jr = new JoinRace(botId, "germany", "xpto", 1);
		// JoinRace jr = new JoinRace(botId, "usa", "xpto", 1);
		// JoinRace jr = new JoinRace(botId, "france", "xpto", 1);
		// send(jr);

		int laneSwitchPieceIndex = -1;
		Integer idealLaneIdx = 0;
		long gameTick = 0L;

		while ((line = reader.readLine()) != null) {
			// parse server message
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);

			boolean isCarPositions = false;
			// process / reply
			if (msgFromServer.msgType.equals("crash")) {
				// extract carId data
				CarId cId = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
				Car car = race.getCar(cId);
				car.crashed = true;
				if (car.equals(myCar)) {
					// assume track is more slippery than expected
					double frictionMod = Race.CRASH_SLIP_FRICTION_MOD * (1 - Math.abs(myCar.slipSpeed) / 50d);
					race.kSlipFriction = race.kSlipFriction * frictionMod;
					Util.log("kSlipFriction=" + race.kSlipFriction + " (*" + frictionMod + ")");
				}
				Util.log("car " + cId + " CRASHED at pIdx=" + car.position.piecePosition.pieceIndex + " laneIdx=" + car.position.piecePosition.lane.endLaneIndex);
			} else if (msgFromServer.msgType.equals("spawn")) {
				// extract carId data
				CarId cId = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
				Car car = race.getCar(cId);
				car.crashed = false;
				Util.log("car " + cId + " SPAWNED at pIdx=" + car.position.piecePosition.pieceIndex + " laneIdx=" + car.position.piecePosition.lane.endLaneIndex);
			} else if (msgFromServer.msgType.equals("yourCar")) {
				// extract carId data
				myCarId = gson.fromJson(gson.toJson(msgFromServer.data), CarId.class);
			} else if (msgFromServer.msgType.equals("carPositions")) {
				isCarPositions = true;

				// extract relevant data
				// Type listType = new TypeToken<List<CarPosition>>() {}.getType();
				// List<CarPosition> newCarPositions = gson.fromJson(gson.toJson(msgFromServer.data), listType);
				final CarPositionsMsg carPositionsMsg = gson.fromJson(line, CarPositionsMsg.class);
				List<CarPosition> newCarPositions = carPositionsMsg.data;
				gameTick = carPositionsMsg.gameTick;

				// update car positions
				race.updateCarMovementData(newCarPositions);

				// currentPiece and position
				CarPosition myCarPosition = myCar.position;
				maxSlipAngle = Math.max(Math.abs(myCarPosition.angle), maxSlipAngle);
				int currentPieceIndex = myCarPosition.piecePosition.pieceIndex;
				TrackPiece currentPiece = race.track.pieces.get(currentPieceIndex);
				TrackPiece nextPiece = race.track.pieces.get((currentPieceIndex + 1) % race.track.pieces.size());

				// pick one action
				double recommendedSpeed = race.getRecommendedSpeedForCar(myCar);
				double speedDifference = myCar.speed - recommendedSpeed;
				myCar.recommendedSpeed = recommendedSpeed;

				// Integer idealLaneIdx = idealLaneIdxForPieceIdx.get(currentPieceIndex + 1);
				int currentLaneIdx = myCarPosition.piecePosition.lane.endLaneIndex;

				// effective power vs drag check in the first few ticks
				if (!checkedEfPower && gameTick > 0 && gameTick <= 4) {
					myCar.throttle = 1.0d;
					send(new Throttle(myCar.throttle));

					if (gameTick > 0 && myCar.acceleration > 0) {
						double kEffectivePower = Math.abs(myCar.acceleration);
						race.kPowerMod = kEffectivePower / 0.2d;
						Util.log("POWER=" + race.kPowerMod);

						checkedEfPower = true;
					}
				}
				// front drag check on beginning of first lap
				else if (!checkedFrontDrag && gameTick > 4 && gameTick < 9) {
					myCar.throttle = 0.0d;
					send(new Throttle(myCar.throttle));

					if (gameTick > 7) {
						double kDrag = Math.abs(myCar.acceleration / myCar.speed);
						Util.log("DRAG=" + kDrag);
						race.kFrontDrag = kDrag;
						checkedFrontDrag = true;
					}
				}
				// process operations normally
				else {
					// check switch if obstacle is in the way on this lane
					if (nextPiece.switchLane && laneSwitchPieceIndex != currentPieceIndex && idealLaneIdx != currentLaneIdx && race.myCarLaneHasObstacle(currentLaneIdx)
							&& (race.canMyCarSwitchToLane(currentLaneIdx + 1) || race.canMyCarSwitchToLane(currentLaneIdx - 1))) {
						if (race.canMyCarSwitchToLane(currentLaneIdx + 1)) {
							// increment laneIdx
							send(new SwitchLane("Right"));
						} else if (race.canMyCarSwitchToLane(currentLaneIdx - 1)) {
							// decrement laneIdx
							send(new SwitchLane("Left"));
						}
						laneSwitchPieceIndex = currentPieceIndex;
					}
					// check switch if not on the fastest lane
					else if (nextPiece.switchLane && laneSwitchPieceIndex != currentPieceIndex && idealLaneIdx != currentLaneIdx && race.canMyCarSwitchToLane(idealLaneIdx)) {
						if (idealLaneIdx > currentLaneIdx) {
							// increment laneIdx
							send(new SwitchLane("Right"));
						} else {
							// decrement laneIdx
							send(new SwitchLane("Left"));
						}
						laneSwitchPieceIndex = currentPieceIndex;
					}
					// going way too fast
					else if (speedDifference >= 0.3d) {
						myCar.throttle = 0.0d;
						send(new Throttle(myCar.throttle));
					}
					// going slightly too fast
					else if (speedDifference < 0.3d && speedDifference >= 0d) {
						if (myCar.hasActiveTurbo(gameTick)) {
							myCar.throttle = 0.0d;
						} else {
							myCar.throttle = 0.1d / race.kPowerMod;
						}
						send(new Throttle(myCar.throttle));
					}
					// going slightly too slow
					else if (speedDifference < 0 && speedDifference >= -0.2d) {
						myCar.throttle = (0.8d / race.kPowerMod);
						send(new Throttle(myCar.throttle));
					}
					// going way too slow
					else {
						// activate turbo if available and not being used
						if (!myCar.crashed && speedDifference < -10.0d && myCar.getAvailableTurbo() != null && !myCar.hasActiveTurbo(gameTick)) {
							send(new ActivateTurbo(myCar.getAvailableTurbo(), gameTick));
							Util.log("TURBO!");
						} else {
							myCar.throttle = 1.0d;
							send(new Throttle(myCar.throttle));
						}
					}
				}
				if (logMovementData) {
					Util.log("piece=" + currentPieceIndex + " (" + currentPiece.angle + " ; " + currentPiece.switchLane + ")" + " thr=" + myCar.throttle + " spd=" + myCar.speed + " maxSpd="
							+ recommendedSpeed + " acc=" + myCar.acceleration + " slipAngle=" + myCar.position.angle + " slipSpeed=" + myCar.slipSpeed + " laneIdx="
							+ myCar.position.piecePosition.lane.endLaneIndex + " tick=" + gameTick);
				}
			} else if (msgFromServer.msgType.equals("turboAvailable")) {
				final TurboAvailableMsg turboAvailableMsg = gson.fromJson(line, TurboAvailableMsg.class);
				Turbo turboAvailable = turboAvailableMsg.data;

				// only add turbo if don't have one already
				if (!myCar.crashed && myCar.getAvailableTurbo() == null) {
					myCar.turboList.add(turboAvailable);
				}
			} else if (msgFromServer.msgType.equals("join")) {
				Util.log("Joined");
				send(new Ping());
			} else if (msgFromServer.msgType.equals("gameInit")) {

				// extract race data
				String raceStr = gson.toJson(msgFromServer.data).replace("{\"race\":", "");
				raceStr = raceStr.replace("}}}", "}}");
				// to avoid switch attribute, as it is a reserved keyword
				raceStr = raceStr.replace("switch", "switchLane");
				race = gson.fromJson(raceStr, Race.class);

				// init race related attributes
				myCar = race.getCar(myCarId);
				race.myCar = myCar;
				race.loadCurvePiecesLength();

				idealLaneIdx = race.getPreferredLaneIndex();

				// Util.log("race : " + gson.toJson(race));
				Util.log("Race init");

			} else if (msgFromServer.msgType.equals("gameEnd")) {
				Util.log("Race end");
				Util.log("maxSlipAngle=" + maxSlipAngle);
			} else if (msgFromServer.msgType.equals("gameStart")) {
				startTime = new Date();
				Util.log(startTime, "Race start");
			} else if (msgFromServer.msgType.equals("createRace")) {
				send(new Ping());
				// join race
			} else {
				send(new Ping());
			}

			// log server msg data
			if (!isCarPositions && msgFromServer != null && msgFromServer.data != null) {
				Util.log(startTime, ">>> " + msgFromServer.msgType + " : " + gson.toJson(msgFromServer.data));
			}
		}
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

// general message classes
abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;

	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class BotId {
	public final String name;
	public final String key;

	BotId(final String name, final String key) {
		this.name = name;
		this.key = key;
	}
}

// specific message classes
class CarPositionsMsg {
	public final String msgType;
	public final List<CarPosition> data;
	public final String gameId;
	public final long gameTick;

	CarPositionsMsg(final String msgType, final List<CarPosition> data, final String gameId, final long gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameId = gameId;
		this.gameTick = gameTick;
	}
}

// specific message classes
class TurboAvailableMsg {
	public final String msgType;
	public final Turbo data;

	TurboAvailableMsg(final String msgType, final Turbo data) {
		this.msgType = msgType;
		this.data = data;
	}
}

class Join extends SendMsg {
	public final String name;

	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}

class CreateRace extends SendMsg {
	public final BotId botId;

	public final String trackName;
	public final String password;
	public final int carCount;

	CreateRace(final BotId botId, final String trackName, String password, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "createRace";
	}
}

class JoinRace extends SendMsg {
	public final BotId botId;

	public final String trackName;
	public final String password;
	public final int carCount;

	JoinRace(final BotId botId, final String trackName, String password, int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.password = password;
		this.carCount = carCount;
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}
}

class SwitchLane extends SendMsg {
	private String value;

	public SwitchLane(String direction) {
		this.value = direction;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class ActivateTurbo extends SendMsg {
	public ActivateTurbo(Turbo turbo, long gameTick) {
		turbo.activationTick = gameTick;
	}

	@Override
	protected Object msgData() {
		return "TURBO!";
	}

	@Override
	protected String msgType() {
		return "turbo";
	}
}
